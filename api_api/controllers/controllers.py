# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request

class ApiApi(http.Controller):
       @http.route("/mobile/session/authenticate", type="json", auth="none")
       def authenticateLogin(self, db, login, password):
           user_id = (
              request.env["res.users"]
              .sudo()
              .search([("active", "in", [True, False]), ("login", "=", login)])
          )
           request.session.authenticate(db, login, password)
           info = {
              "userID": user_id.id,
              "name": user_id.name,
              "login": user_id.login,
          }
           return info
       @http.route('/api_api_read',type='json',auth='public')         
       def fileRead(self,domain=[]):
          model= request.env['api_api.api_api']
          data= model.search(domain).read()
          return data

       @http.route('/api_api_write',type='json',auth='public')
       def filewrite(self,**post):
          record = request.env['api_api.api_api'].search([("id",'=',post.get('id'))])
          record.write({'name':post.get('name',record.name),'Quantity':post.get('Quantity',record.Quantity)})
          return '{Record updated successfully}'
